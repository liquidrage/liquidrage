---
layout: post
title:  "Example of Design"
date:   2020-05-13  
comments: false
---


========================================================================================
## Unordered Lists
========================================================================================

* One

========================================================================================

1 One

========================================================================================

{% highlight css %}

Manan Goel


{% endhighlight %}
========================================================================================

<kbd>standout</kbd> **more when** `applying`

========================================================================================
<div markdown="0"><a href="#" class="btn">Primary Button</a></div>

========================================================================================

**Watch out!** You can also add notices by appending `{: .notice}` to a paragraph.
{: .notice}

### Standard Code Block

{% raw %}
    <nav class="pagination" role="navigation">
        {% if page.previous %}
            <a href="{{ site.url }}{{ page.previous.url }}" class="btn" title="{{ page.previous.title }}">Previous article</a>
        {% endif %}
        {% if page.next %}
            <a href="{{ site.url }}{{ page.next.url }}" class="btn" title="{{ page.next.title }}">Next article</a>
        {% endif %}
    </nav><!-- /.pagination -->
{% endraw %}

Manan Goel